package com.example.likelionhospitalreview.service;

import com.example.likelionhospitalreview.domain.dto.JoinRequest;
import com.example.likelionhospitalreview.domain.dto.JoinResponse;
import com.example.likelionhospitalreview.domain.entity.User;
import com.example.likelionhospitalreview.exception.ErrorCode;
import com.example.likelionhospitalreview.exception.JoinException;
import com.example.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public JoinResponse addNewUser(JoinRequest joinRequest) {
        userRepository.findByUserId(joinRequest.getUserId()).ifPresent((sth)->{
            throw new JoinException(ErrorCode.DUPLICATE_ID);
        });
        User savedUser = userRepository.save(joinRequest.toEntity());
        return JoinResponse.fromEntity(savedUser);
    }
}
