package com.example.likelionhospitalreview.exception;

import com.example.likelionhospitalreview.domain.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionManager {

    @ExceptionHandler(JoinException.class)
    public ResponseEntity<?> joinExceptionHandler(JoinException e) {
        return ResponseEntity.status(e.getErrorCode().getHttpStatus())
                .body(Response.fail(e.getErrorCode().getMessage()));
    }
}
