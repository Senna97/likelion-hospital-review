package com.example.likelionhospitalreview.controller;

import com.example.likelionhospitalreview.domain.dto.JoinRequest;
import com.example.likelionhospitalreview.domain.Response;
import com.example.likelionhospitalreview.domain.dto.JoinResponse;
import com.example.likelionhospitalreview.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    public Response<JoinResponse> joinUser(@RequestBody JoinRequest joinRequest) {
        JoinResponse joinResponse = userService.addNewUser(joinRequest);

        return Response.success(joinResponse);
    }
}
