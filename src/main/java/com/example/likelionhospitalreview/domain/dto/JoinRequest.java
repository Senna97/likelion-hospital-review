package com.example.likelionhospitalreview.domain.dto;

import com.example.likelionhospitalreview.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JoinRequest {

    private String userId;
    private String password;
    public User toEntity() {
        return User.builder()
                .userId(userId)
                .password(password)
                .build();
    }
}
