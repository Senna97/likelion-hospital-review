package com.example.likelionhospitalreview.domain.dto;

import com.example.likelionhospitalreview.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JoinResponse {
    private String userId;

    public static JoinResponse fromEntity(User user) {
        return new JoinResponse(user.getUserId());
    }
}
